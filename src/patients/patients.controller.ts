import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  ValidationPipe,
  UsePipes,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import {
  CreateFamilyDetailsDto,
  CreatePatientSignupDetailsDto,
  CreatePersonalDetailsDto,
  RequestConverterPipe,
  ValidateMongoId,
} from './dto/index';
import { UpdatePatientDetailsDto } from './dto/patient-details-update.dto';
import { Patient } from './entities/patient.entity';
import { PatientsService } from './patients.service';

@Controller('patient')
export class PatientsController {
  constructor(private readonly PatientService: PatientsService) {}
  @Get('all')
  getAllPatientDetails(): Promise<Patient[]> {
    return this.PatientService.findAll();
  }
  @Get(':id')
  getPatient(@Param('id', ValidateMongoId) id: string): Promise<Patient> {
    return this.PatientService.findOne(id);
  }
  @Post('signup')
  signUp(
    @Body(ValidationPipe) signupDetails: CreatePatientSignupDetailsDto,
  ): Promise<CreatePatientSignupDetailsDto> {
    return this.PatientService.addSingupDetails(signupDetails);
  }
  // @UsePipes(ValidationPipe)
  @Post('details/personal/:id')
  addPersonalDetails(
    @Param('id', ValidateMongoId) id: string,
    @Body(ValidationPipe) personaDetails: CreatePersonalDetailsDto,
  ): Promise<CreatePersonalDetailsDto> {
    return this.PatientService.addPersonalDetails(id, personaDetails);
  }
  @UsePipes(RequestConverterPipe)
  @Post('details/family/:id')
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'aadharFront', maxCount: 1 },
      { name: 'aadharBack', maxCount: 1 },
      { name: 'insuranceFront', maxCount: 1 },
      { name: 'insuranceBack', maxCount: 1 },
    ]),
  )
  addFamilyDetails(
    @Param('id', ValidateMongoId) id: string,
    @Body(ValidationPipe) familyDetails: CreateFamilyDetailsDto,
    @UploadedFiles()
    files: {
      aadharFront?: Express.Multer.File[];
      aadharBack?: Express.Multer.File[];
      insuranceFront?: Express.Multer.File[];
      insuranceBack?: Express.Multer.File[];
    },
  ): Promise<CreateFamilyDetailsDto> {
    return this.PatientService.addFamilyDetails(id, familyDetails, files);
  }

  @UsePipes(RequestConverterPipe)
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'aadharFront', maxCount: 1 },
      { name: 'aadharBack', maxCount: 1 },
      { name: 'insuranceFront', maxCount: 1 },
      { name: 'insuranceBack', maxCount: 1 },
    ]),
  )
  @Put(':id')
  updateDetails(
    @Param('id', ValidateMongoId) id: string,
    @Body() patientDetails: UpdatePatientDetailsDto,
    @UploadedFiles()
    files: {
      aadharFront?: Express.Multer.File[];
      aadharBack?: Express.Multer.File[];
      insuranceFront?: Express.Multer.File[];
      insuranceBack?: Express.Multer.File[];
    },
  ): Promise<UpdatePatientDetailsDto> {
    return this.PatientService.update(id, patientDetails, files);
  }

  @Delete(':id')
  removeDetails(@Param('id', ValidateMongoId) id: string) {
    return this.PatientService.delete(id);
  }
}
