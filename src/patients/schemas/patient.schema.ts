import mongoose from 'mongoose';

export const PatientSchema = new mongoose.Schema({
  email: { type: String, unique: true },

  password: { type: String },

  firstName: { type: String },

  lastName: { type: String },

  mobile: { type: Number },

  dob: Date,

  age: { type: Number },

  weight: { type: Number },

  height: { type: String },

  bmi: { type: Number },

  isPatientDiabetic: { type: Boolean },

  isPatientHaveCardiac: { type: Boolean },

  isPatientAwareOfBP: { type: Boolean },

  fatherName: { type: String },

  fatherAge: { type: Number },

  fatherCountryOfOrigin: { type: String },

  motherName: { type: String },

  motherAge: { type: Number },

  motherCountryOfOrigin: { type: String },

  isParentsDiabetic: { type: Boolean },

  isParentHaveCardiac: { type: Boolean },

  AreParetnsAwareOfBP: { type: Boolean },

  documents: {
    aadharFront: {
      fileName: { type: String },
      path: { type: String },
      mimeType: { type: String },
      fileSize: { type: Number },
    },
    aadharBack: {
      fileName: { type: String },
      path: { type: String },
      mimeType: { type: String },
      fileSize: { type: Number },
    },
    insuranceFront: {
      fileName: { type: String },
      path: { type: String },
      mimeType: { type: String },
      fileSize: { type: Number },
    },
    insuranceBack: {
      fileName: { type: String },
      path: { type: String },
      mimeType: { type: String },
      fileSize: { type: Number },
    },
  },
});
