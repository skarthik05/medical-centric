import { Patient } from '../entities/patient.entity';

export class PatientUtils {
  public CaliculateBMI(weight: number, height: number): number {
    height = height * 30.48;
    return +(weight / Math.pow(height / 100, 2)).toFixed(2);
  }

  public ageFromDateOfBirthday(dateOfBirth: Date): number {
    const today = new Date();
    const birthDate = new Date(dateOfBirth);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    return age;
  }

  public compareBothPassword(
    password: string,
    confirmPassword: string,
  ): boolean {
    return password === confirmPassword;
  }

  public attachFileInformation(files: any, patientDetalils: any): Patient {
    const { aadharFront, aadharBack, insuranceFront, insuranceBack } =
      files || {};
    patientDetalils['documents'] = {};
    if (aadharFront?.length) {
      patientDetalils.documents['aadharFront'] = {
        fileName: aadharFront[0].filename,
        path: aadharFront[0].path,
        mimeType: aadharFront[0].mimetype,
        fileSize: aadharFront[0].size,
      };
    }
    if (aadharBack?.length) {
      patientDetalils.documents['aadharBack'] = {
        fileName: aadharBack[0].filename,
        path: aadharBack[0].path,
        mimeType: aadharBack[0].mimetype,
        fileSize: aadharBack[0].size,
      };
    }
    if (insuranceFront?.length) {
      patientDetalils.documents['insuranceFront'] = {
        fileName: insuranceFront[0].filename,
        path: insuranceFront[0].path,
        mimeType: insuranceFront[0].mimetype,
        fileSize: insuranceFront[0].size,
      };
    }
    if (insuranceBack?.length) {
      patientDetalils.documents['insuranceBack'] = {
        fileName: insuranceBack[0].filename,
        path: insuranceBack[0].path,
        mimeType: insuranceBack[0].mimetype,
        fileSize: insuranceBack[0].size,
      };
    }
    return patientDetalils;
  }
}
