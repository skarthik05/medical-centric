import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  CreateFamilyDetailsDto,
  CreatePatientSignupDetailsDto,
  CreatePersonalDetailsDto,
  UpdatePatientDetailsDto,
} from './dto/index';
import { Patient } from './entities/patient.entity';
import { PatientSchema } from './schemas/patient.schema';
import { PatientUtils } from './utils/patient.utils';
@Injectable()
export class PatientsService {
  constructor(
    @InjectModel('patient')
    private readonly PatientModal: Model<typeof PatientSchema>,
    private readonly patientUtils: PatientUtils,
  ) {}
  async findAll(): Promise<Patient[]> {
    return await this.PatientModal.find({}, '-password -__v').lean();
  }

  async findOne(id: string): Promise<Patient> {
    return await this.PatientModal.findById(id, '-password -__v').lean();
  }

  async addSingupDetails(
    patientSignupDto: CreatePatientSignupDetailsDto,
  ): Promise<any> {
    if (
      !this.patientUtils.compareBothPassword(
        patientSignupDto.password,
        patientSignupDto.confirmPassword,
      )
    )
      throw new HttpException("Password didn't match", 409);
    try {
      const { confirmPassword, ...cred } = patientSignupDto;
      const newSignup = await this.PatientModal.create({
        ...cred,
      });
      return newSignup._id;
    } catch (error) {
      throw new HttpException('Email is already exist', HttpStatus.CONFLICT);
    }
  }
  async addPersonalDetails(
    id: string,
    patientPersonalDetailsDto: CreatePersonalDetailsDto,
  ): Promise<any> {
    patientPersonalDetailsDto.age = this.patientUtils.ageFromDateOfBirthday(
      patientPersonalDetailsDto.dob,
    );
    patientPersonalDetailsDto.bmi = this.patientUtils.CaliculateBMI(
      patientPersonalDetailsDto.weight,
      +patientPersonalDetailsDto.height,
    );
    const personalDetails = await this.PatientModal.findByIdAndUpdate(
      id,
      patientPersonalDetailsDto,
      { new: true },
    );
    return personalDetails;
  }
  async addFamilyDetails(
    id: string,
    patientFamilyDetailsDto: CreateFamilyDetailsDto,
    files: any,
  ): Promise<any> {
    patientFamilyDetailsDto = this.patientUtils.attachFileInformation(
      files,
      patientFamilyDetailsDto,
    );

    const familyDetails = await this.PatientModal.findByIdAndUpdate(
      id,
      patientFamilyDetailsDto,
      { new: true },
    ).exec();
    return familyDetails;
  }

  async update(
    id: string,
    patientDetails: UpdatePatientDetailsDto,
    files: any,
  ): Promise<any> {
    if (patientDetails.email) {
      const isEmailTaken = await this.PatientModal.findOne({
        email: patientDetails.email,
      });
      if (isEmailTaken)
        throw new HttpException('Email is already taken', HttpStatus.CONFLICT);
    }
    patientDetails = this.patientUtils.attachFileInformation(
      files,
      patientDetails,
    );
    return await this.PatientModal.findByIdAndUpdate(id, patientDetails);
  }

  async delete(id: string): Promise<any> {
    return await this.PatientModal.findByIdAndDelete(id).exec();
  }
}
