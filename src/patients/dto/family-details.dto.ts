import { ArgumentMetadata, PipeTransform } from '@nestjs/common';
import { Transform } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class CreateFamilyDetailsDto {
  @IsNotEmpty()
  @IsString()
  fatherName: string;
  @IsNotEmpty()
  @IsString()
  fatherCountryOfOrigin: string;
  @IsNotEmpty()
  @IsNumber()
  fatherAge: number;
  @IsNotEmpty()
  @IsString()
  motherName: string;
  @IsNotEmpty()
  @IsNumber()
  motherAge: number;
  @IsNotEmpty()
  @IsString()
  motherCountryOfOrigin: string;
  @IsNotEmpty()
  @IsBoolean()
  isParentsDiabetic: boolean;
  @IsNotEmpty()
  @IsBoolean()
  isParentHaveCardiac: boolean;
  @IsNotEmpty()
  @IsBoolean()
  AreParetnsAwareOfBP: boolean;
}
export class RequestConverterPipe implements PipeTransform<Object> {
  transform(value: any, metadata: ArgumentMetadata): Object {
    // Optional casting into ObjectId if wanted!
    if (value?.fatherAge) value.fatherAge = Number(value.fatherAge);

    if (value?.motherAge) value.motherAge = Number(value.motherAge);

    if (value?.isParentHaveCardiac)
      value.isParentHaveCardiac = Boolean(value.isParentHaveCardiac);
    if (value?.isParentsDiabetic)
      value.isParentsDiabetic = Boolean(value.isParentsDiabetic);
    if (value?.AreParetnsAwareOfBP)
      value.AreParetnsAwareOfBP = Boolean(value.AreParetnsAwareOfBP);

    return value;
  }
}
