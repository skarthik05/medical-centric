import {
  IsNotEmpty,
  IsNumber,
  IsDateString,
  IsString,
  IsBoolean,
  IsPhoneNumber,
  IsMobilePhone,
} from 'class-validator';
export class CreatePersonalDetailsDto {
  @IsNotEmpty()
  @IsString()
  firstName: string;
  lastName: string;
  @IsNotEmpty()
  @IsNumber()
  mobile: number;
  @IsNotEmpty()
  @IsDateString()
  dob: Date;
  @IsNotEmpty()
  @IsNumber()
  weight: number;
  @IsNotEmpty()
  @IsString()
  height: string;
  @IsNotEmpty()
  @IsBoolean()
  isPatientDiabetic: boolean;
  @IsNotEmpty()
  @IsBoolean()
  isPatientHaveCardiac: boolean;
  @IsNotEmpty()
  @IsBoolean()
  isPatientAwareOfBP: boolean;
  age: number;
  bmi: number;
}
