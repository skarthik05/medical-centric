import {
  ArgumentMetadata,
  PipeTransform,
  HttpException,
} from '@nestjs/common';
import {
  IsEmail,
  IsNotEmpty,
  IsAlphanumeric
} from 'class-validator';
import { ObjectId } from 'mongodb';
export class CreatePatientSignupDetailsDto {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsAlphanumeric()
  password: string;

  @IsNotEmpty()
  @IsAlphanumeric()
  confirmPassword: string;
}

// @Injectable()
export class ValidateMongoId implements PipeTransform<string> {
  transform(value: string, metadata: ArgumentMetadata): string {
    // Optional casting into ObjectId if wanted!
    if (ObjectId.isValid(value)) {
      if (String(new ObjectId(value)) === value) return value;
      throw new HttpException('Invalid Id', 403);
    }
    throw new HttpException('Invalid Id', 403);
  }
}
