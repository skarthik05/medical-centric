import { HttpException, HttpStatus, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { PatientsController } from './patients.controller';
import { PatientsService } from './patients.service';
import { PatientSchema } from './schemas/patient.schema';
import { PatientUtils } from './utils/patient.utils';
import { diskStorage } from 'multer';
import { extname } from 'path';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'patient', schema: PatientSchema }]),
    MulterModule.register({
      storage: diskStorage({
        destination: function (req, file, cb) {
          cb(null, './upload');
        },
        filename: function (req, file, cb) {
          const uniqueSuffix = ` ${Date.now()}-${Math.round(
            Math.random() * 1000,
          )}`;
          cb(
            null,
            `${file.fieldname}-${uniqueSuffix}${extname(file.originalname)}`,
          );
        },
      }),
      fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
          // Allow storage of file
          cb(null, true);
        } else {
          // Reject file
          cb(
            new HttpException(
              `Unsupported file type ${extname(file.originalname)}`,
              HttpStatus.BAD_REQUEST,
            ),
            false,
          );
        }
      },
    }),
  ],
  controllers: [PatientsController],
  providers: [
    PatientsService,
    {
      provide: PatientUtils,
      useClass: PatientUtils,
    },
  ],
})
export class PatientsModule {}
