export class Patient {
  email: string;
  password: string;

  firstName: string;
  lastName: string;
  mobile: number;
  dob: Date;
  weight: number;
  height: string;
  isPatientDiabetic: boolean;
  isPatientHaveCardiac: boolean;
  isPatientAwareOfBP: boolean;
  age: number;
  bmi: number;

  fatherName: string;
  fatherCountryOfOrigin: string;
  fatherAge: number;
  motherName: string;
  motherAge: number;
  motherCountryOfOrigin: string;
  isParentsDiabetic: boolean;
  isParentHaveCardiac: boolean;
  AreParetnsAwareOfBP: boolean;
  documents?: Array<Object>;
}
